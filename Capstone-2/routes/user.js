const express = require("express")
const router = express.Router();
const userController = require("../controllers/user");
const productController = require("../controllers/user")
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// for users to register an account
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// for users who wants to access their account (User Authentication)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/details", auth.verify, (req, res) => {
  const userId = auth.decode(req.headers.authorization).id // Access the user ID from the decoded token
  const userFirstName = auth.decode(req.headers.authorization).firstName
  const userLastName = auth.decode(req.headers.authorization).lastName
  userController.getProfile(userId, userFirstName, userLastName).then(resultFromController => res.send(resultFromController));
});

router.post("/order", auth.verify, (req, res) => {



let data = {
	// User ID will be retrieved from the request header
	userId : auth.decode(req.headers.authorization).id,
	// Course ID will be retrieved from the request body
	
	productId : req.body.productId,
	
	
}

	userController.order(data).then(resultFromController => res.send(resultFromController));

});

router.get("/registeredUser", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	userController.getAllUser(userData.isAdmin).then(resultFromController => res.send(resultFromController))
});


// To change user as an admin
router.patch("/:userId/update", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.updateUser(req.params, req.body, userData.isAdmin).then(
		resultFromController => res.send(resultFromController))
});

router.get("/:userId", (req, res) => {
	
	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	//  We can however retrieve the course ID by accessing the request's "params" property which contains all parameters provided via the URL

	userController.retrieveUser(req.params).then(resultFromController => res.send(resultFromController));

})


module.exports = router;