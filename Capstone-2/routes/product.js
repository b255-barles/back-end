const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");
const userController = require("../controllers/user");

// Route for creating a product
// Note that the only account that can create a product is an admin
router.post("/create", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	/*user = userData.isAdmin;*/
	productController.addProduct(req.body, userData.isAdmin).then(resultFromController => res.send(resultFromController));
		
})

router.get("/all", (req,res) => {
	productController.retrieveAllProduct().then(resultFromController => res.send(resultFromController))
});

router.get("/active", (req,res) => {
	productController.retrieveAllActiveProduct().then(resultFromController => res.send(resultFromController))
});

router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	//  We can however retrieve the course ID by accessing the request's "params" property which contains all parameters provided via the URL

	productController.retrieveProduct(req.params).then(resultFromController => res.send(resultFromController));

})

router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.updateProduct(req.params,req.body, userData.isAdmin).then(resultFromController => res.send(resultFromController))
})
router.put("/archive/:productId", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
  
	/*user = userData.isAdmin;*/console.log("isAdmin?",userData.isAdmin)
	productController.archiveProduct(req.params, req.body, userData.isAdmin,userData).then(resultFromController => res.send(resultFromController));
		
})

// Backend API Route
router.put("/products/:productId/updateQuantity", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (!userData.isAdmin) {
    return res.status(403).send("Not authorized");
  }

  const { productId } = req.params;
  const { quantity } = req.body;

  Product.findByIdAndUpdate(
    productId,
    { quantity: quantity },
    { new: true }
  )
    .then((product) => {
      if (product) {
        console.log('Product updated:', product);
        res.status(200).send(product);
      } else {
        res.status(404).send("Product not found");
      }
    })
    .catch((error) => {
      console.log(error);
      res.status(500).send("Server error");
    });
});


// router.get("/:userId", auth.verify, (req, res) => {
// // const userData =  auth.decode(req.headers.authorization)
// // console.log(productId)
// let data = {
// 	// User ID will be retrieved from the request header
// userId : auth.decode(req.headers.authorization)
// 	// Course ID will be retrieved from the request body
// 	productId : req.body.productId,
// 	productName : req.body.productName,
// 	quantity : req.body.quantity

// }

// productController.userOrder(req.params,userData).then(resultFromController => res.send(resultFromController));
// })

module.exports = router;