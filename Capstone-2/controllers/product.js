const Product = require("../models/Product");
const auth = require("../auth");
const User = require("../models/User");
const bcrypt = require("bcrypt");

// CREATING A NEW PRODUCT
// Products should be created by an admin account only

module.exports.addProduct = (reqBody, userData) => {
  if (userData === false) {
    return Promise.resolve("Not an admin");
  } else {
    let newProduct = new Product({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
    });
    // Save the created object to our database
    return newProduct
      .save()
      .then((product) => {
        // Product creation successful
        return product;
      })
      .catch((error) => {
        console.log(error);
        // Product creation failed
        return null;
      });
  }
};

module.exports.retrieveAllProduct = () => {
	return Product.find({}).then(result => {
		
		return result;
	})
}

module.exports.retrieveAllActiveProduct = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.retrieveProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}
module.exports.updateProduct = (reqParams, reqBody, userData) => {
  // Specify the fields/properties of the document to be updated
  if (userData === false) {
    return Promise.resolve("Not an admin");
  } else {
    let updateProduct = {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
      isActive: reqBody.isActive, // Include isActive field if needed
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateProduct, { new: true })
      .then((product) => {
        // Product is not updated
        if (!product) {
          return false;
        }
        // Product updated successfully
        return true;
      })
      .catch((error) => {
        console.log(error);
        return false;
      });
  }
};


module.exports.archiveProduct = (reqParams, reqBody, isAdmin) => {
	console.log('archiveProduct called with:', reqParams, reqBody, isAdmin);
  if (!isAdmin) {
    return Promise.resolve("Not an admin");
  } else {
    const { productId } = reqParams;
    const { isActive } = reqBody;

    return Product.findByIdAndUpdate(productId, { isActive }, { new: true })
      .then((product) => {
        if (product) {
          console.log('Product updated:', product);
          return { isActive: product.isActive }; // Return the updated isActive value
        } else {
          return { isActive };
        }
      })
      .catch((error) => {
        console.log(error);
        return { isActive };
      });
  }
};

/*module.exports.retrieveAllOrders = () => {
	return Product.find({}).then(result => {
		return result;
	})
}*/
