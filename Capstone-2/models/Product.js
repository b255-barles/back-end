const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name : {
		type: String,
		required : [true, "name of the product is required"]
	},

	description : {
		type : String,
		required : [true, "description is required"]
	},

	price : {
		type : Number,
		required : [true, "price of the product is required"]
	},

	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		// The "new Data()" expression instantiates a new "Date" taht stores the current date and time whenever a course is created in our database
		default : new Date()
	},

	userOrders : [{
		userId : {
			type: String,
			required : [true, "UserID is required"]
		},

		orderId : {
			type : String
		}

	}]



})

module.exports = mongoose.model("Product", productSchema);