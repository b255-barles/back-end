const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required : [true, "First Name is required"]
	},
	lastName : {
		type: String,
		required : [true, "Last Name is required"]
	},
	email : {
		type: String,
		required : [true, "Email is required"]
	},
	mobileNo : {
		type: Number,
		required : [true, "mobile Number is required"]
	},

	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
		
	},

	orderedProduct : [{
		products : [{
			productId : {
				type : String,
				required : [true, "product Id is required"]
			},

			productName : {
				type : String,
				required : [true, "Name of the product is required"]
			},

			quantity : {
				type : String,
				required : [true, "Number of  product to order is required"]
			}
		}]
	}]



});
module.exports = mongoose.model("User", userSchema);