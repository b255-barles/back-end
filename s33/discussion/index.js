console.log("HELLO");

// [SECTION] Getting all posts

// The Fetch API allows you to asynchronously request for a resource or data
//  A "promise" is an object that represents the eventual completion or failure of an asynchronous function and it resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));


// Retrieve all posts following the REST API 
// By using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
// the 'fetch' method will return a promise that resolves to a response object
// the "then" method capture the respinse object and returns another promis whch will be eventuall resolve or rejected
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
// use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from the "fetch" request
// using multiple "then" methods creates a "promise chain" 
.then((json) => console.log(json));
//  the "async" and "await" keywords is another approach that can be used to acieve asynchronous code
// Used in functions to indicatewhich portions of code should be waited for
// creates an asynchronous function
async function fetchData(){
	// waits for the "ffetch" method to complet then store the value in the rsult variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	// Result returned by fetch returns a promise
	console.log(result);
	// the returned response is an object
	console.log(typeof result);
	// We cannot access the content f response bt directly accessing its body property
	console.log(result.body);

	// convers the data from the "response" object as JSON
	let json = await result.json();
	// print out the content
	console.log(json);
}

fetchData();

// [SECTION] Getting a specific post

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Creating a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the request object ot POST following the REST API
	//  Default method is get
	method: 'POST',
	//  Sets the header data of the request object to be sent to the backend
	// specified that the content will be in a JSON structure
	headers: {
		'Content-type': 'application/json',

	},
	// sets the content/body data of the "Request" object ot be sent to the backend
	//  JSON.stringify converts tje obejct data into a stringified JSON
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',

	},

	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: 'Hello Again',
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));

//  [SECTION] UPdating a post using PATCH

// Updates a specific post following the REST API
// The difference between PUT and PATCH is the numbr of propertie being changed
//  Patch is used to update the whole object
//  pUT is used to update a single/several properties

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',

	},
	body: JSON.stringify({
		title: 'Corrected post'

	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Deleting a specific post following the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});

// [SECTION] Filtering posts

// The data can be filtered by sending the userID along with the URL
// Information sent via the url can be done by adding the question mark sybol(?)

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json)=> console.log(json));

// [SECTION] Retrieving nested/related comments to posts

// Retrieving comments for a specific post follwing the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));