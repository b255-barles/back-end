// Getting fetch to retrieve all to do list

fetch('https://jsonplaceholder.typicode.com/todos')

.then((response) => response.json())
.then((json) => console.log(json.map((item) => {
	return item.title

	
})));


// creating fetch request using the Get method that will retrieve sinle to do list item 

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/2')
.then((response) => response.json())
.then((json) => console.log(json))

//  creating post method

fetch('https://jsonplaceholder.typicode.com/todos', {
	
	method: 'POST',

	headers: {
		'Content-type': 'application/json',

	},
	
	body: JSON.stringify({
		completed: false,
	
		title: "Created To Do List Item",
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json))


// PUT

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',

	},

	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list with a different data structure",
		
		status: "Pending",
		title: "Updated to Do list item",
		userId: 1 

	})
})

.then((response) => response.json())
.then((json) => console.log(json));


// PATCH

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',

	headers: {
		'Content-type': 'application/json',

	},

	body: JSON.stringify({
		dateCompleted: "03/27/23",
		
		status: "Complete",
		
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json));


// DELETE

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});
