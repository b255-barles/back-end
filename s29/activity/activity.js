//finding users with letter s in their first name or d in their last name
db.users.find({ $or : [{firstName: {$regex: "s", $options: "$i"}}, {lastName: {$regex: "d", $options: "$i"}}]},

	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}

);


// finding users who have none in company and age gt or equal to 70

db.users.find({ $and : [{company : "none"}, {$gte: {70}}]

});


// finding users with the letter e in their firstname and has an age of lt or equal to 30

db.users.find({ $and: [{firstName: {$regex: "e", $options: "$i"}}, {age: {$lte : 30}}]});