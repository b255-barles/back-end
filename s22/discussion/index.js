console.log("Hello");


// array methods

// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items

// Mutator methods

/*
	- mutator methids are functions that mutate or change an array after theyre created
	- these methods manipulate the orig array performing various tasks such as adding and removing elements

*/
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit']

// push()
/*
	- adds am element in the end of an array and  returns tghe arrays length

*/

console.log('Current Array: ');
console.log(fruits);

let fruitsLength = fruits.push('Mango'); /*to get or store the length of an array*/

console.log(fruitsLength);
console.log("Mutated aray from push method: ");
console.log(fruits);

// adding multiple element to an array

fruits.push("Avocado", "Guava");
console.log("Mutated aray from push method: ");
console.log(fruits);

// pop()
/*
	- removes the last element in an array and returns the removed element

*/
let removedFruit = fruits.pop(); /*To check what part of the array has been removed*/
console.log(removedFruit);
console.log("Mutated aray from pop method: ");
console.log(fruits);

// unshift()

/*
	-adds an element at the beginnign of an array 
*/

fruits.unshift('Lime', 'Banana'); /*will be added infront of the array*/
console.log("Mutated aray from unshift method: ");
console.log(fruits);

// shift()
/*
	-Removes an element at the beginning of an array and returns the removed element
*/

let anotherFruit = fruits.shift(); 
console.log(anotherFruit);
console.log("Mutated aray from shift method: ");
console.log(fruits);

// splice()
/*
	- simultaneously removes element from specified index number and adds elements 
*/

fruits.splice(1,2, 'Lime', 'Cherry');/*yung 1 and 2 assigns kung pang ilang index niya ipapalit si lime and cherry*/
console.log("Mutated aray from splice method: ");
console.log(fruits);


// sort()
/*
	-Rearranges the array elements in alphanumeric order
*/

fruits.sort();
console.log("Mutated aray from sort method: ");
console.log(fruits);

/*N
	Important Note: 
	 - The "sort" method is used for more complicated sorting functions
	 
*/
// reverse()
/*
	- reverses the order of array elements

*/

fruits.reverse()
console.log("Mutated aray from reverse method: ");
console.log(fruits);

// non-mutator methods
/*
	- non-mutator methods are functions that do not modift or change an array after they're created
	- these methods do not manipulate original aray performing various tasks such as returning elements from an array and combining arrays
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
/*
	-returns the index number of the first matching element found in an array
	-if no match was found, the result will be -1 
	- the search process will be done from the first elemetn proceeding to the last element
*/

let firstIndex = countries.indexOf('PH');
console.log("Results  of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log("Results  of indexOf method: " + invalidCountry);


// lastIndexOf()
/*
	-returns the index number of the last matching element found in an array
	-The search process will be done from last element proceeding to the first element
*/

let lastIndex = countries.lastIndexOf('PH');
console.log("Results  of lastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log("Results  of lastindexOf method: " + lastIndexStart);

// slice()
/*
	-Portions/Slice elemetns from an arrray and returns a new array

*/

let slicedArrayA = countries.slice(2)
console.log("Results  of slice method : ");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Results  of slice method : ");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Results  of slice method : ");
console.log(slicedArrayC);


// toString()
/*
	-returns an array as a string separated by commas
*/

let stringArray = countries.toString();
console.log("Results  of toString method : ");
console.log(stringArray);

// concat()
/*
	- combines 2 arrays and returns the combined result
*/

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Results  of concat method : ");
console.log(tasks);

// combining multiple arrays
console.log('Results from concat method: ');
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// Combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log("Results  of concat method : ");
console.log(combinedTasks);


// join()
/*
	-returns an array as a string separated by specified separator string
*/

let users = ['john', 'jane', 'joe', 'robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration Methods
/*
	- iteration methods are loops designed to perform repetitive tasks on arrays
	-Iteration methods loops over all items in an array 
	- useful for manipulating array data resulting in complex tasks
	- array iteration methods normally work with a function suppliedas an argument
	-how these function works is by performing tasks that are pre-defined within an array's method
*/

// forEach()
/*
	- similar to a for loop that iterates on each array element
	- for each item in the array, the anonymous function passed in th forEach() method will bbe run
	- the aninymous function is able to receive the current item being iterated or loop over by assigning a parameter
	- its common practice to use the singular form of the array content for parameter names used in array loops. 
*/

allTasks.forEach(function(task){
	console.log(task);
})

// Using forEach with conditional statements
let filteredTasks = [];
allTasks.forEach(function(task){

	if(task.length > 10){ /*kukuhain niya yung words na may more than 10 letters or characters*/
		filteredTasks.push(task);
	}
})

console.log("Results  of filtered tasks : ");
console.log(filteredTasks);


// map()
/*
	-iterates on each element and returns new array with different values depending on the result of the functions operations
	-this is useful for performing tasks where mutating/changing the elements are required
	-unlike the forEach method, th map mehod requires the use of a "return" statement in order to create another array with the performed operation
*/
let numbers =[1, 2, 3, 4, 5];

let numberMap = numbers.map(function(numbers){
	return numbers * numbers;
})

console.log("original");
console.log(numbers); /*original array unaffected by map()*/
console.log("Result");
console.log(numberMap); // A new array is returned by map()

// map() vs. forEach

let numberForEach = numbers.forEach(function(numbers){

	return numbers * numbers
})

console.log(numberForEach);

// forEach(), loops over all items in the array as does map() but forEach() does not return a new array


// every()
/*
	- checks if all elements in an aarray meet the given condition
	-this is useful for validating data stored in arrays especially when dealing with large amounts of data
	-returns a true value if all elements meet the condition and false f otherwise
*/

let allValid = numbers.every(function(numbers){

	return (numbers < 3);
})
console.log("Results  of every method : ");
console.log(allValid);

// some()
/*
	-Checks if at least one element in the array meets the given condition
	-returns a true value if at least one element meets the condition and false if otherwis
*/


let someValid = numbers.some(function(numbers){

	return (numbers < 2);
})
console.log("Results  of some method : ");
console.log(someValid);

// filter()
/*
	-returns new array that contains elements wc meets the given condition
	- returns an empty array f no elements were found 
	-useful fo filtering array elements with a given condition and shortens the syntex compared to uusing to other array iteration methods
	-mastery of loops can gelp us work effectively by reducing the amound of code we use
*/

let filterValid = numbers.filter(function(numbers){
	return (numbers < 3);
})

console.log('Result')
console.log(filterValid);

// no elemetns found

let nothingFound = numbers.filter(function(number){
	return (number = 0)
});

console.log('Result')
console.log(nothingFound);

// filtering using forEach
let filteredNumbers = [];

numbers.forEach(function(numbers){
	if(numbers < 3){
		filteredNumbers.push(numbers)
	}
})
console.log("result filter method");
console.log (filteredNumbers);

// includes()
/*
	-includes() method checks if the argument passed can be found in the array 
	-it returns a boolean wc can be saved in a variable
	 	-returns true if the argument is found in the array
	 	-returns false if it is not
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes('Mouse');
console.log(productFound1);

let productFound2 = products.includes('Headset');
console.log(productFound2);

/*
	methods can be chained using them one after another 
	- the result of the first method is used on the second method until all chained methods have been resolved
*/

let filteredProducts = products.filter(function(products){
	return products.toLowerCase().includes('a');
})

console.log(filteredProducts);

// reduce()
/*
	- Evaluates elements form left to right and returns/reduces the array into a single value
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){

	// used to track the current iteration count and accumulator/currentValue data
	console.warn('Current iteration ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// the operation to reduce the array into a single value

	return x + y;
})

console.log('Result of reduce method' + reducedArray);

// reducing string arrays

let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x,y){
	return x + ' ' + y
})

console.log('Result reduce method: ' + reducedJoin);