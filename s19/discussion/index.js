// console.log("HELLO")


// Conditional Statements allow us to control the flow of our program.. It allows uus to run a statements/instruction if a condition is met or run another separate instruction if otherwise

// [SECTION] if, else if, else statements

let numA = -1;

if(numA < 0){

	console.log("HELLO");
}

// The result of the expression added in the if's condition must result



console.log(numA<0);  //results o true and son the if statements is run


numA = 0;

if(numA < 0){
	console.log("Hello Again if numA is 0!s")
}
// IT will not run because the expression now is false

let city= "New York";

if(city === "New York"){
	console.log("Welcome to New York city")
}

/*
	Executes a statement if prev condtions are false and if the specified conditions is true

	the "else if" clause is optional and can be added to capture additional conditions to change the flow of a prog
*/


let numH = 1;

if(numA < 0){
	console.log('Hello')
}	else if (numH > 0){
	console.log("World")
}

// We were able to run the else if() statement after we evaluated that the if condition was failed

// if the i() condition was passed and run, we will no longer eval the elseif() abd end the process there

numA = 1;

if(numA > 0){
	console.log("Hello");
}else if (numH > 0){
	console.log("World");
}

// else if() no lnger ran becuase the if statements

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York city")
}else if(city === "Tokyo"){
	console.log("Welcome to Tokyo Japan")
}


//  Since we failed the condition for the frst if(), we went to the else if() and checked and passed that condtion


// else Statement
/*
	Executes a statement if all other conditions are false

	tthe "else" statement is optional and can be addd to capture any other result to change the flow of the program

*/


if(numA < 0){
	console.log("Hello")
}	else if (numH === 0){
		console.log("World")
}		else {
		console.log('Again')
}

// Else statements should only be added if there is a preceeding if condtion. else statements by itself will not work, however, if statements will work even if there is no else statement.



//  if, else if and else statements with functions

/*
	Most of the time we would like to use if, else if and else statements with functions to control the flow of our application

	by including them inside functions, we can decide when certain condtions will be checked instead of executing statements when the JS

	The return statement can be utilized with conditional statements in combination with functions to change values to be used for other features
*/


let message = 'No Message';
console.log(message);

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return 'Not a typhoon yet';
	}	
	else if (windspeed <=61){
		return 'Tropical Depression Detected'
	}
	else if (windspeed >=62 && windspeed <= 88){
		return 'Tropical Storm Detected';
	}
	else if (windspeed >=89 || windspeed <= 117){
		return 'Severe Tropical Storm Detected'
	}
	else {
		return 'typhoon Detected';
	}
}

message = determineTyphoonIntensity(110);
console.log(message);

// MINI ACTIVITY
// Create a fucntion with and if else statement inside
// The function should test for if a givent number is an even number or an odd number

function getEvenOdd (num1){
	num2 = num1 % 2;
	if (num2 === 0){
		console.log(num1 + " is an even number")
	}
	else {
		console.log(num1 + ' is an Odd Number')
	}
}

answer = getEvenOdd(23);


// Truthy Examples

/*
	If the result of an exression in a condition results to a truthy calue, the condition returns true and the corresponding statemets are executed.

	expressions are any unit of code that can be eval to avalue
*/

if (true){
	console.log('Truthy')
}

if (1) {
	console.log('Truthy')
}
if([]) {
	console.log('Truthy');
}

// Falsy examples
if(false){
	console.log('Falsy')
}
if(0){
	console.log('Falsy')
}
if(undefined){
	console.log('Falsy')
}

// [SECTION] Conditional (Ternary) operator

/*
	The conditional (ternary) operator takes in 3 operands
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy
		- can be used as an alt to an "if else" statement
		- commonly used for a single line executionwhere the result consists of only one line ofcode
	
	Syntax:
	(expression) ? ifTrue : ifFalse;


*/

//  SIngle statement execution

let ternaryResult =(1<18) ? true : false;
console.log("result of ternary operator: " + ternaryResult);

// Multi statement execution

let name /*= prompt("what is your name");*/
function isOfLegalAge(){
	name ='john'
	return ' you are of the legal age limit'
}

function isUnderAge(){
	name ='jane'
	return ' you are under the age limit'
}

/*
	- input received from the prompt function is returned as a strung data type
	-the "parseInt" function converts the input received into a number data type
	- NaN(Not A Number)
*/

let age = parseInt(prompt("What is your age?"));
/*console.log(age)*/
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result: " + legalAge + ", " + name);


// [SECTION] Switch statements

/*
	- The switch statement evaluates an expresseion and matches the expresions value to a case clause. The swithc will then execute the statements associated with that case as well as statemetns in cases that follow the match

	- switch cases are considered as "loops" meaning it will compare te expression with each of the case values until a match is found

	- the "break" statement is used to terminate the current loop once the match has been found
	

*/

let day = prompt("What day of the week is it today?")
	.toLowerCase();
console.log(day);

switch (day){
	case 'monday':
		console.log("The color of the day is red");
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is viole");
		break;
	
	default:
		console.log("Plese input valid day")
		break;
}

//  [SECTION] Try-catch-finally statement
/*
	-"try catch" statements are commonly used for error handling
	-There are instances when the application returns an error/warning that is not necessarily an error in the context of ourcode
	-These errors are a result of an attempt of the programming language to help developers in creating effecient code
	-they are used to specify a response whenever an exception/error is received
*/


function showIntensityAlert(windspeed){
	try{
		alerat(determineTyphoonIntensity(windspeed));

	// err are commonly used variable names used by developers for storing errors
	} catch (error) {
		// The type of operator is used to check the data type of a value and returns a string value of what the data type is
		console.log(typeof error);

		// Catch errors within a try statement
		// In this case the error is an unknown function 'alerat' which doesnt exist in JS
		// error.message is used to access the info relting to an error obj.
		console.warn(error.message);
	} finally {
		// Continue execution of code regardless of success and failure of code executon in the 'try' block to handle/resolve err
		alert('intensity updates will show new alert')
	}
}

showIntensityAlert(56);