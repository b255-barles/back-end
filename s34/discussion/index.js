// use the "require" directives to load the express module/package
// A "module" is a software component or a part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us access to methods and functions that will allow us to create a server
const express = require("express")
// Create an application using express
// Thus creates an express application and stores this in a constant called app
//  in layman's term app is our server 
const app = express()

// For our application server to run, we need a port to listen
const port = 4000;

// Setup for allowing theserver to handle data from requests
// Allows your app to read json data
//  Methods used from express JS are middlewares
// Middleware is a software that prvides common services and capabilities to applications outside of what's offfered bt the operating system
app.use(express.json());

// allows your app to read adata from forms
// by default, information recieved from the URL can only be received as a string or an array
// by applying  the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughoout our application
app.use(express.urlencoded({extended:true}));

// [SECTION] Routes
// Express has a methods corresponding to each HTTP method

app.get("/", (req, res) => {
	res.send("hello world")
});

// create another GET route
app.get("/hello", (req, res) =>{
	res.send("hello from the hello endpoint")
});

// create a POST route
app.post("/hello", (req, res) => {
    // req.body contains the content/data of the request body
    // All the properties defined in our Postman request will be accessible here as properties 
	res.send(`Hello There ${req.body.firstName} ${req.body.lastName}`)
});
//  An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock database
let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	//  if contents of the "request.body" with the property "userName" and "password" is not empty 
	if(req.body.userName !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.userName} successfully registered`);
	} else{
		res.send("Please input BOTH username and password")
	}
});

// Create PUT route to change password of a specific user
app.put("/change-password", (req,res) => {

	let message;

	// Creates a for loop that will loop through each element of the array
	for(let i=0; i < users.length; i++){
		//  If the username provided in the postman client and the username of the current object in the loop is the same

		if(req.body.userName == users[i].userName){
			users[i].password = req.body.password;
			//  Changes password of the user found by the loop
			users.splice(i, 1);
			message = `User ${req.body.userName}'s password has been updated`;
			//  Breaks out of the loop once a user that matches the user provided is found
			
			break;
		} else {
			message = "user does not exist";
		}
	}

	res.send(message);
	console.log(users);

})



// Tells our server to listen to the port
// If the port is accessed, we can run the server
//  Returns a message to confirm that the server is running inthe terminal

// if(require.main) would allow us to iten to the app directly if is not imported to another module, it will run the app directly
// else, if it is needed to be imported, it will nor run the app and instead export it to be used in another file
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;

