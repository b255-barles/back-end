const express = require("express")
const app = express()
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/home", (req,res) => {

	res.send("Welcome to the home page")
});

let users =[];
app.post("/signup", (req, res) => {
	console.log(req.body);

	//  if contents of the "request.body" with the property "userName" and "password" is not empty 
	if(req.body.userName !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.userName} successfully registered`);
	} else{
		res.send("Please input BOTH username and password")
	}
});
app.get("/users", (req, res) => {
	res.send(users);
})

app.delete("/delete-user", (req,res) => {

	let message;

	// Creates a for loop that will loop through each element of the array
	for(let i=0; i < users.length; i++){
		//  If the username provided in the postman client and the username of the current object in the loop is the same

		if(req.body.userName == users[i].userName){	
			message = `User ${req.body.userName} has been deleted`;
			users.splice(i, 1);
			break;
		}
		if(req.body.userName !== users[i].userName){
			message = "User does not exist"
		}


		else {
			message = "no users found"
		}
	}

	

	res.send(message);

})













if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;



