// ARray in programming is simply a list of data. let's write the example earlier

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']

// [SECTION] Arrays

/*
	-arraysa are ised to store mltiple related values in a single variable
	-They are declared using square brackets[] also known as "ARRAY literals"
	-commonly used to store numerous amounts of adata to manipulate in order to perform a nmber of task
*/

// Common examples for arrays

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write arrays
let myTasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sass'
	]

// Creating an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);

// [Section] Length Property

// .length property allows us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// length property can also be used with strings. some array methods and properties can also be used with strings

let fullName = 'Jamie Noble'
console.log(fullName.length);

// Length propert can also set the total number of items in an array, meaning we can acually delte the last item in the array

// myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks)


/* if (myTasks.length = 4){
	myTasks.length = myTasks.length-1;
	continue;
 }

 else if{
	myTasks = myTasks.length
 }
*/

// to delete a specific item in an array we can eploy array methods(next session)

// Another example of using decrement
cities.length--;
console.log(cities)

fullName.length = fullName.length-1;
console.log(fullName.length)
fullName.length--;
console.log(fullName);

// If you can shorten the array by settling the length property, you can also lengthern it by adding a number into the lenght property

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++
console.log(theBeatles);

// [SECTION] Reading from arrays
/*
	-Accessing array elements is one of the more common tasks that we do with an array
	-This can be done through the use of array indexes
	-each element in an array is associated with its own index/number
	-In JS, the first element is associated with the number 0 and increasing this number by 1 for every proceeding element
	-the reason an array starts with 0 is due to how the language is designed
	-Array indexes actually refer to an address/lcation in the devices meory and how the info is stored
	- Example array location in memory
		Array[0] = 0X7ffe947bad0
		Array[1] = 0X7ffe947bad4
		Array[2] = 0X7ffe947bad8
*/

console.log(grades[0]);
console.log(computerBrands[0]);
// Acccessing an array element that does not exist will return "undefined"

console.log(grades[20]);

let lakersLegend = ['Kobe', 'Shaq', 'LeBron', 'Magic', 'Kareem'];
console.log(lakersLegend[1]);
console.log(lakersLegend[3]);


// you can save/store array items in another varaible

let currentLaker = lakersLegend[2];
console.log(currentLaker);

// You can also reassign array values using the items indices

console.log("before");
console.log(lakersLegend);
lakersLegend[2] = 'Pau Gasol';
console.log("AFTER");
console.log(lakersLegend);

// accessing the last element of an array
// since the first element of an array starts with 0, subtracting 1 to the lenght of an array will offset the value by one allowing us to get the last index

let bullLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
let lastElementIndex = bullLegends.length-1;
console.log(bullLegends[lastElementIndex]);

// you can also add it directly
console.log(bullLegends[bullLegends.length-1]);

// Adding items into the array 

let newArr = []
console.log(newArr[0]);

newArr[0] = 'Cloud Strife';
console.log(newArr);

console.log(newArr[1]);N
newArr[1] = 'Tifa Lockhart';
console.log(newArr);

// you cam also add items at the end of the array

// newArr[newArr.length-1] = "Aerith Gainsborough";
newArr[newArr.length] = "Barret Wallace"
console.log(newArr);


//  looping over an array
// You can use a for loop to iterate over all items in an array

// Set the counter as the index and set a condition that  as long as the current idexed iterated is less than the lenght of the array

for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}


let numArr = [5,12,30,46,40];
console.log(numArr);

for (let i = 0; i< numArr; i++){
	if(numArr[i] % 5 === 0 ){
		console.log(numArr[i] + " is divisible by 5");

	} else {
		console.log(numArr[i] + "is not divisible by 5");
	}
}


// [SECTION] Multidimensional Array
/*
	-mulidimentsional array are useful for storing complex data structures

	- a practical app of this is to help visualize/create real world obj

	- through useful in a number of cases, creating complex array structures is not always recommended


*/

let chessBoard = [
	['a1','b1','c1','d1','e1','f1','g1','h1',],
	['a2','b2','c2','d2','e2','f2','g2','h2',],
	['a3','b3','c3','d3','e3','f3','g3','h3',],
	['a4','b4','c4','d4','e4','f4','g4','h4',],
	['a5','b5','c5','d5','e5','f5','g5','h5',],
	['a6','b6','c6','d6','e6','f6','g6','h6',],
	['a7','b7','c7','d7','e7','f7','g7','h7',],
	['a8','b8','c8','d8','e8','f8','g8','h8',]

]

console.log(chessBoard);

// acessing elements of multidimensional array
console.log(chessBoard[1][4]);

console.log("pawn moves to: " + chessBoard[1][5]);