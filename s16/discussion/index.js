console.log('hello world');


// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;

console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


// Assignment operators

	// Basic asignment operators (=)

	let assignmentNumber = 8;

	// Addition assignment operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to thevariable

	assignmentNumber = assignmentNumber + 2;
	console.log ('Result of addition assignment operator: ' + assignmentNumber);

	// Shorthand 

	assignmentNumber += 2;
	console.log ('Result of addition assignment operator: ' + assignmentNumber);

	assignmentNumber -=2;
	console.log ('Result of subtraction assignment operator: ' + assignmentNumber);

	assignmentNumber *=2;
	console.log ('Result of multiplication assignment operator: ' + assignmentNumber);

	assignmentNumber /=2;
	console.log ('Result of division assignment operator: ' + assignmentNumber);

//  Multiple Operators and Parentheses

	/*
		When multiple operators are applied in a single statemnets, it follows the PEMDAS
		
	*/

	let mdas = 1 + 3 -3 * 4 / 5;
	console.log ('Result of mdas operations: ' + mdas);

	let pemdas = 1 + (2-3) * (4/5);
	console.log ('Result of pemdas operations: ' + pemdas);


// Increment and Decrement 

	let z = 1;
	let increment  = ++z;

	console.log ('Result of increment: ' + increment);

	let decrement = --z;
	console.log ('Result of decrement: ' + decrement);

// Type Coercion

	/*
		- Type coercion is the automatic or implicit conversion of values from one data type to another
		- This happends when operations are performed on different data types that would normally not be possible and yield irregular results.
		- Values are automatically converted from one data type to another in order to resolve operations




	*/
	let numA = '10';
	let numB = 12
	//  Adding/concatenating a string and a number will result in a string

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numE = true + 1;
	console.log(numE);

	let numF = false + 1
	console.log(numF);

// comparison operators 

	let juan = "juan"

	// equality operator (==)
	/*
		checks wheter the operands are equal/have the same content

		retuns a boolean value 
	*/

	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(0 == false);
	// compares two strings that are the same
	console.log('juan' == 'juan');
	// compares a string with the varaible "juan" declared above

	console.log('juan' == juan);

	// Inequality operator

	/*
		-checks whether the operand are not equal/have different content
	*/
	console.log("START OF INEQUALITY OPERATOR")
	console.log( 1 != 1);
	console.log( 1 != 2);
	console.log( 1 !='1');
	console.log(0 !=  false);
	console.log('juan' != 'juan');
	console.log('juan' != juan);

	// Strict equality operator
	/*
		- checks whether the operands are equal/ have the same content
		- Also compare the data types of 2 values
		-JavaScript is a loosely typed language 
		meaning that values of differnet data types can be stored in variables 
		-This sometimes can cause problems within our code
	*/
	console.log("START OF STRICT EQUALITY OPERATOR")
	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log('juan' === juan);


	//Strict inequality operator
	console.log("START OF STRICT INEQUALITY OPERATOR")
	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== false);
	console.log('juan' !== 'juan');
	console.log('juan' !== juan);

// Relational Operators

	// Some comparison operators checks whether one value is greateror less than to the other value

	let a = 50;
	let b = 65;

	// GT or Greater Than operator (>)
	let isGreaterThan = a > b;
	// LT or Less Than operator (<)
	let isLessThan = a < b;
	// GTE or Greater Than or Equal to (>=)
	let isGTorEqual = a >= b;
	// LTE or Less Than or Equal to (<=)
	let isLTorEqual = a <= b;
	console.log('START')
	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);

// Logical Operators 

	let isLegalAge = true;
	let isRegistered = false;

	// Logical And Operator (&&)
	// Retues true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered
	console.log("Result of logical operator: " + allRequirementsMet)

	// Logical OR operator (||)
	// Returns true if 1 of the operands are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet)

	// Logical NOT operator (!)
	// Returns the opposite value 
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " + someRequirementsNotMet)

