const Course = require("../models/Course");
// const course = require("../routes/course")
const auth = require("../auth");
const User = require("../models/User");
const bcrypt = require("bcrypt");

// create a new course
/*
	steps:
	1. create a new course object using the mongoose model and the information from the request body and the id from the header
	2. save the new course to the database
*/

module.exports.addCourse = (reqBody,userData) => {
	// Create a variable newCourse and instantiates a new "Course" object using the mongoose model
	// console.log(User ({ isAdmin : reqBody.isAdmin}))
		
		
		if(userData == false){
			return Promise.resolve(false);
		}else {
		let newCourse = new Course({
    			name : reqBody.name,
    			description : reqBody.description,
    			price : reqBody.price
    		});
    		// save the created object to our database
    		return newCourse.save().then((course, error) => {
    			// course creation failed
    			if(error) {
    				return false;
    			// course creation successful
    			}else {
    				return true;
    			}
    		});
		}
	}
/*	});
}*/

// Retrieve all courses
/*
		Steps:
		1. Retrieve all the courses from the database


*/	
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}
// Retrieve all AcTIvE courses
/*
		Steps:
		1. Retrieve all the courses from the database with the property ofisActive to truue

*/
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}
// retrieving a specific course
/*
	Steps:
	1. retrieve teh coursre that matches the course ID providedfrom the URL
*/
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
/*
	Steps:
	1. Create a variable updateCourse which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the params property and the variable "updateCourse" containing the information from the request body
*/

// Information to update a course will be coming form both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {
		// Specify the fields/properties of the documetn to be updated
	let updateCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		// Course is not updated
		if (error){
			return false;
		// Course updated successfully
		}else{
			return true;
		}
	})
}

module.exports.archiveCourse = (reqParams, reqBody, userData) => {
	if(userData == false){
			return Promise.resolve(false);
	}else {
		let archiveCourse = {
		isActive : reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		// Course is not updated
		if (error){
			return false;
		// Course updated successfully
		}else{
			return true;
		}
		})


	}
}
	   		
 

    		
    	

    


	
