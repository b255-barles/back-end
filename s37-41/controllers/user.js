//  The "User" Variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

// Check if email already exists
/*
	Steps: 
	1. Use mongoose "find" method to duplicate emails
	2. Use the "then" method to send a respond back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the frontend via the "then" method found in the route file 
	return User.find({email : reqBody.email}).then(result => {
		console.log(result)
		//  The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
			// No duplicate email found
			//  The user is not yet registered in the database
		}else {
			return false;
		}
	})
};

// User registration
/*
	Steps:
	1. Create a new user object using hte mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	// CReates a variable "newUser" and instantiates a new "User" object using the mongoose model
	//  uses the information from the request body to provide all the necessary infromation
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)

	})
	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error) {
			return false;
		}else {
			return true;
		}
	})
}

// User authentication 
/*
	Steps:
	1. check the database if the user email exists
	2. compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	// The "findOne" method returns the firt record in the collection that matches the search criteria
	// We use the "findOne" method instrad of the "find" method which returns all records that match the same criteria
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the "createCAccessToken" method defined in the "atuh.js" file
				// returning an object back to the frontend application is common practice to ensure infromation is properly labeled
				return{access : auth.createAccessToken(result)}
			}else {
				// passwords do not match
				return false;
			}
		}
	})
}

// Retrieve user details
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Reassign the password of the returned document to an empty string
			3. Return the result back to the frontend
		*/


module.exports.getProfile = (reqBody) => {
	return User.findOne({_id: reqBody._id}).then(result =>{
		
		if(result == null){
			return false;
		}else {
			result.password = "";
			/*let user = User({
				firstName : result.firstName,
				lastName : result.lastName,
				email : result.email,
				mobileNo: result.mobileNo,
				password : " "
				
			});*/

			return result;
		}
	})
}

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating before returing a response back to the frontend
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the user's enrollments array
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})
		let isCourseUpdated = await Course.findById(data.courseId).then(course =>{

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false
	}

};