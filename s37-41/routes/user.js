const express = require("express")
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// route for checking ig the user's email already exis in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" pproperty of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//  Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//  Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// route for getting user information
router.get("/details", auth.verify, (req, res) => {
	// Uses the decode method defined inthe auth.js file to retrieve user information from token passng the token from the request heder as an argumetn
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(req.body).then(
		resultFromController => res.send(resultFromController))
})

router.post("/enroll", auth.verify, (req, res) => {

let data = {
	// User ID will be retrieved from the request header
	userId : auth.decode(req.headers.authorization).id,
	// Course ID will be retrieved from the request body
	courseId : req.body.courseId
}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

});


// Allows us to export the "router" object that will be accessed in our in our "index.js" file

module.exports = router;
