// CRUD Operations

// INsert Document (CREATE)
/*
	Syntax:
		insert one document 
			db.collectionName.insertOne({
				"fieldA": "vlaueA",
				"fieldB": "valueB"
			});

		Insert Many Documents
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"company": "none"
});

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"company": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"company": "none"
	}
]);

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript"
		"isActive": true

	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": true
	}
]);


// Find Documents (Read'Retrieve)

db.users.find(); // this wo;; retrieve all the documents
db.users.find({
	"firstName" : "Jane"
});

db.users.find({
	"firstName" : "Neil",
	"age": 82
});


db.users.findOne({}); // Returns the first document in our collection

db.users.findOne({
	"firstName": "Stephen"
});

// Update documents

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"company": "none"
});

// updating one document

db.users.updateOne({
	"firstName": "Test"
	},
	{
	$set:{
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "bilgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}


);

// Removing a field
db.users.updateOne(

	{
		"firstName": "Bill"
	},
	{
		$unset:{
			"status" : "active"
		}
	}
);

// Updating Multiple Documnets
db.users.updateMany(
	{
		"company":"none"
	},
	{
		$set: {
			"company":"HR"
		}
	}
);


db.users.updateOne(
	{},
	{
		$set:{
			"company": "Operations"
		}
	}

);

// Deleting Documents(Delete)
db.users.insertOne({

	"firstName":"Test"
});

db.users.deleteOne({
	"firstName":"Test"
});

db.users.deleteMany({
	"company": "comp"
});

db.users.insert({
    firstName: "John",
    lastName: "Smith",
    age: 21,
    contact: {
        phone: "87654321",
        email: "johnsmith@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});

