console.log("Hello");

// [SECTION] JSON objects
/*
	-JSON stands for JavaScript Object Notation
	-JSON is also used in other programming languages hence the name JavaScript Object Notation
	-Core JS has a built JSON object that contains methods for parsing JSON objects and converting strings into JS objects
	- JS objects are not to be confused with JSON
	-serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
	-A byte is a unit of data that is eight binary digits( 1 and 2 ) that is used to represent a character (letters, numbers or typographic symbols)
	-Bytes are infromation taht a computer processes to perform different tasks
*/

// JSON objects
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}
*/

// JSON arrays
/*"cities" : [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]*/

// [SECTION] JSON methods
// The JSON objets contains methods for parsing and converting data into stringifies JSON

// [SECTION] Converting Data into stringified JSON
/*
	- Stringified JSON is a JS object converted into a string to be used in other functions of a JS application
	-They are commonly used in HTTP requests where info is required to be sent and received in a stringified JSON format
	-Requests are an important part of programming where an application communicates with a backednd application to perform different tasks such as getting/ creating data in a database
	-A frontend application is an application that is used to interact with users to perform differetn visual tasks and display information while backedn applications are commonly used for all the business logic and data processing
	- A database normally stores infromation/data that can be used in most app
	- Commonly stored data in databases are user information, transaction records and product inforamtion
	-Node/Express JS are two types of technologies that are used for creating backedn application which processes requests from frontend application
*/

let batchesArr = [{batchName: "Batch X"}, {batchName: "Batch Y"}];

console.log("Result from stringify method: ");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address:{
		city: 'Manila',
		country: 'Philippines'
	}
});
console.log(data);

// [SECTION] usign stringify method with Variables
/*
	- when information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	- The property name and value would have the same name which can be confusing for beginniers 
	-This is done on purpose for code readability meaning when an information is stored in a variable and when the object created to be stringified is created, we supply the variable name instraad of a hard coded value
	-This is commonly used when the information to be stored and sent to a backend application will come from a frontend application
*/
/*
let firstName = prompt('What is your first name? ');
let lastName = prompt('What is your last name? ');
let age = prompt('What is your age? ');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('which country does your address belong to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})
console.log(otherData);*/

// [SECTION] Converting stringified JSON into JavaScript objects
/*
	- Objects are common data types used in application because of the complex data structure that can be created out of them
	- Information is commonly sent to application in stringified JSON and thenconverted back into objects 
	- THis happens bith for send information to backend application
*/

/*let batchesJSON = '[{"batchName": "Bacth X"}, {"batchName": "Batch Y"},{"address": " "},{"geo": {"lat": "29.4572","lng": "-164.2990"}}]';

console.log("Result from the parse method");
console.log(JSON.parse(batchesJSON));*/
let users = 
'[{"id": "10","name": "Clementina DuBuque","username": "Moriah.Stanton","email": "Rey.Padberg@karina.biz",                    "address": {"street":"Kattie Turnpike","suite": "Suite 198","city": "Lebsackbury","zipcode": "31428-2261"},                   "geo": {"lat": "-38.2386","lng": "57.2232"},                                                                                "phone": "024-648-3804", "website": "ambrose.net",                                                                           "company": {"name": "Hoeger LLC","catchPhrase": "Centralized empowering task-force", "bs": "target end-to-end models"}}]'
    


      
      console.log(JSON.parse(users));





