console.log("Hello");

// [SECTION] While Loop

/*
	- A while Loop takes in an expression/condition
	-Expression are any unit of code tat can be eval to a value
	- IF the condition evaluates to true, the statements inside thecode block will be executed

	- A statement is a command the programmer gives to the computer

	- a loop will iterate a certain number of times until an expression/condtion is met

*/

let count = 5;

// While the value of count is not equal to 0
while(count !== 0){
	// The current value of count is printed out
	console.log("While: " + count);
	// Decreases the count by 1 after every iteration to stop the loop when it reaches 0

	// Make sure that expression/condtions in loops have their corresponding increment/decrement operators to stop the loop
	count--;
}

// [SECTION] Do while loop

/*
	- A do-while loop works a lot like a while loop but unlike while loops, do-while loops guarantee that the code will be executed at least once.
*/


/*
let number = Number(prompt("Give me a number: "))

do {

	console.log("Do while: " + number)

	number += 1;
} while (number < 10);*/


// [SECTION] For Loops

/*
	- A for loop is more flexible than  while and dowhile loops.
	1. The initializtaion value that will track the progression of the loop
	2. the expression/condtion that will be evaluated wv will determine whether the loop will run one more time
	3. The finalExpression indicates how to advance the loop
*/

/*
for(let count = 0; count <= 20; count++){
	console.log(count);
}*/

let myString = "alex";
// Characters in string may be counted using .length properly
console.log(myString.length);

// accessing elements of a string 
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);


for(let x = 1; x < myString.length; x++){
	console.log(myString[x]);
}


// mini activity
// Write a for loop that will print numbers from 1 - 10
// it must be inside a func

function numbers(){
	for(let x = 0; x < 11; x++)
	console.log(x);
}

numbers();

console.log ("START");
let myName = "ANTONIO";
let x = '';

for(let i = 0; i < myName.length; i++){
	// console.log(myName[i].toLowerCase())

	if(
		myName[i].toLowerCase() == "a" ||  myName[i].toLowerCase() == "e" ||myName[i].toLowerCase() == "i" || myName[i].toLowerCase() == "o" || myName[i].toLowerCase() == "u"
	){
	     continue;
	}	
	else {
		
		x = x + myName[i];
	}
}
console.log(x)

// [SECTION] Continue and Break statements 
/*
	- The continue statemnts allows the code to go to the next iteration of the loop wihtout finishing the execution of all statements 
	- the break statement is sued to terminate the current loop once a match has been found
*/

for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0
	if(count % 2 === 0){
		// tells the code to continue to the next iteration of the loop
		// THis ignores all statemnets located after the continue statement
		continue;
	}
	 console.log("C and B: " + count);



if (count > 10){
	// Tells the code to stop the loop even if the expression/condition of the loop defines that is should execute
	// number values after 10 (except 11) will notbe printed
	break;
	}
}

let name = "alexandro"

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === 'a'){
		console.log("continue to the next iteration");
		continue;
	}

	if(name[i] == "d"){
		break;
	}

}

  
