


// First Part sum

function addNum (num1, num2){

	sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}

addNum(5,15);

// Second part differnce

function subNum (num1, num2){

	difference = num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference);
}

subNum(20,5);

//  Third Part Product

function multiplyNum (num1, num2){
	
	let productNum = num1 * num2;
	console.log("The product of " + num1 + " and " + num2 + ":")
	return productNum;
}

let product = multiplyNum ('50', '10')


console.log(product);


// Fourth part Quotient

function quotientNumber (num1, num2){
	let quotientNum = num1 / num2;
	console.log("The quotient of " + num1 + " and " + num2 + ":")
	return quotientNum;
}

let quotient = quotientNumber ('50', '10')


console.log(quotient);


// Fifth part Circle

function getCircleArea (num1){
	let squared = num1 ** 2;
	let area = 3.1416 * squared;
	console.log ("The result of getting the area of a circle with " + num1 + "radius is")
	return area;
}

let circleArea = getCircleArea(15);
console.log(circleArea);


// Sixth part Average

function getAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4)/ 4;
	console.log ("The average of " + num1 + ',' + num2 + ',' + num3 + ' and ' + num4 + ":");
	return average;

}

let averageVar = getAverage(20,40,60,80);
console.log(averageVar);

// seventh part pass or fail

function checkIfPassed (num1){

	let isPassed = (num1/50) * 100
	console.log ("Is " + num1 +"/50 a passing score?");
	
	return isPassed >= 75;
}

let isPassingScore = checkIfPassed(38);

console.log(isPassingScore);

/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}