// Functions
	// Parameters and Arguments
	// Functions in JS are lines/block of codes that tell our device/app to perform certan tasks when they are called.
	// Functions are mostly created to create complicated tasks to run several lines of code in succession


	

	// Function printInput(){
	// 	let nickname = prompt ("Enter your nickname: ");
	// 	console.log ("Hi, " + nickname);
	// }

	// printInput();

	// For other cases, functions can also process data direcly passed into it instead of relying on global variables and prompt
	function printName(name){
		console.log("My name is " + name);
	}

	printName("Juana");

	// You can drectly pass data into the function. The function can then call/use that data which is referred as " name "

	// "name" is a parameter
	//  A "parameter" acts as a named variable/container that exists only iside of a function
	// It is used to store information that i provided to a function when it is called/ invoked


	// Variables can also be passed as an argument

	let sampleVariable ="yui"
	printName(sampleVariable);

	// Function arguments cannot be used by a function if there are no parameters provided within a function

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log (" THe remainder of " + num + " divided by 8 is: " + remainder)

		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

	// Functions as Arguments 

	// Functions parameters can also accept other functions as arguments 

	// Some complex functions use other functions as arguments to perform more complicated results

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed");
	}

	function invokeFunction(argumentFunction) {
		argumentFunction();
	}


	//  Adding and removing parentheses "()" impacts the output of JS heavily
	// When a function is used with parentheses (), it denotes calling a function
	// A func used w/o a () is normally assoc. with using the func a an arguments to amother func
	invokeFunction(argumentFunction);

	// finding info about a func in the console.log()
	console.log(argumentFunction);

	// Using multiple parameters
	// Multiple "arguments" will correspond to the number of "parameters" declared in a func in succeeding order

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName)
	}

	createFullName('Juan', 'Dela', 'Cruz');

	// "Juan" will be stored in the parameter firstName
	// "Dela" will be stored in the parameter middleName
	// "Cruz" will be stored in the parameter lastName

	//  In JS, providing more/less arguments than the expected parameters will not return an error

	// Providing less arguments than the expected parameters wii be undefined

	createFullName('Juan', 'Dela')
	createFullName('Juan', 'Dela', 'Cruz', 'Hello')
// Using Var as argumetns 

	let firstName  = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	// Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be rec. in the same order it was passed

	// The return statement
	// The return statement allows us to output a value from a function to be passed to te line/block of code that invoked/called a function

	function returnFullName (firstName, middleName, lastName){
		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This message will not be printed")
	}



	// in our example, the "returnFullName" function was callled in the same line as declaring a variable.
	// Whatever value is returned from the returnFullname" varible

	let completeName = returnFullName ("Jeff", "Amazon", "Bezos");
	console.log(completeName)

	// This way, a funciton is able to return a val we can further use/manipulate in our program insted ofo nly printing/displaying it in the console

	// MINI ACTIIVITY
	// Create a function that will add 2 numbers together
	// You must use parameters and arguments

	function addNum (num1, num2){
		let sumNum = num1 + num2
		console.log("The sum of "+ num1 + "and " + num2 + "is: " + sumNum)
	}

	addNum(2,3);

	// NItice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored becaise ot ends the function execution

	// In this example, console.log() will print the returned value of the returnFullName

	console.log(returnFullName(firstName, middleName, lastName));

	// You can also create a variable inside the function to contain the result and return that variable instead.
	function retunAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress;
	}

	let myAddress = retunAddress ('Cebu city', "cebu")

	console.log(myAddress);

	// On the other hand, whn a function only has console.log() to display its reults it will return undefined instead

	function printPlayerInfo(username, level, job){
		console.log("username: " + username);
		console.log("level: " + level);
		console.log("job: " + job);
	}

	let user1 = printPlayerInfo("Knight_White", 95, "Paladin")
	console.log(user1);

	// Returns undefined because printPlayerInfo returns nothing. It only console.logs the details

	// you cant save any value from printPlayerInfo() because it doesnt returnn anything

	






