console.log("Hello World");

// [SECTION] Objects
/*
	- An object is a data type that is used to represent real world objects
	- Ii is a collection of rea;ated data and/or functionalitites
	- In Javascropt, most core JS featuers like strings and arrayd are objects(String are a collection of data)
	- Info stored in objects are represented in a "key:value" pair
	- A "key" is also mostly referred to as  a "property" of an object
	- Different data types may be stored in an objcects property creating complex data structures


*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999,

};

console.log("Result from creating objects using intializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.name);
console.log(cellphone.manufactureDate);


// Creating objects using a constructor function
/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurance of any object which emphasize on the distinct/unique indtity of it
*/



// This is an object
// The "this" keyword allows to assign a new object's properties bu associating them with values recieved from a construction function parameter
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

//  the "new" operator creates an instance of an objects
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using objects constructors");
console.log(laptop);

let mylaptop = new Laptop('Macbook Air', 2020);
console.log("Result from creating objects using objects constructors");
console.log(mylaptop);

// creating empty objects

let computer = {}
let myComputer = new Object()

// [SECTION] Accessing object properties


let array = [laptop, mylaptop]
// May be confused for accessing array indexes
console.log(array[0]['name']);
// This tells us that array[0] is an object by using the dot notation
console.log(array[0].name);

// [SECTION] Initializing/Adding/Deleting/Reassigning Object Properties
/*
	- Like any other variable in JS, objcts may have their properties initialized/added after the object was created/declared
	- This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};



car.name = "Honda Civic"
console.log("Result from addign properties using dot notation");
console.log(car);
console.log(car.name);

/*
	- while using the square bracket this will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can obly be accessed usin the square bracket notation 
*/
// Initializing/adding object property using dot notation
car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log("Result from adding properties using square bracket notation");
console.log(car);


// Deleting object properties

delete car['manufacture date'];
console.log("Result from deleting properties");
console.log(car);


// Reassigning object properties
car.name = 'Dodge Charger R/T';
console.log('Reusult from reassigning properties');
console.log(car);

// [SECTION] Object Methods
/*
	- A methodis a function which is a property of an object
	- They are also functions and one of the key differences they have is that methods are functions related to a specific  object
	-Methids are useful for creating object specific functions whic are used t perform taks on them
*/

let person = {
	name : "John",
	talk : function(){
		console.log('Hello my name is ' + this.name);
	}
}
console.log(person);
console.log('Result from object methods: ')
person.talk();

// Adding  methods to objects 
person.walk = function(){
	console.log(this.name + ' Walk 25 steps forward')
};
person.walk();

// mini - activity

// addd a method to the person object
// That method should be able to console.log a hobby being done by john
// use the this keyword to access the name john

person.hobby = function(){
	console.log(this.name + ' Hobbies are playing basketball and online games')
};
person.hobby();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		state: "Texas"	
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log("hello my name is " + this.firstName + ' ' + this.lastName)
	}
};

friend.introduce();

// [SECTION] Real world application of objects
/*
	-scenario
		1. we would like to create a game that would have several pokemon interact with each other 
		2. Every pokemon would have the same set of stats, properties and functions
*/

// Using object literals to create multiple kinds of pokemon would be time consuming

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokempnhealth");
	},
	faint: function(){
		console.log("pokemon fainted")
	}
}

console.log(myPokemon);

// Creatuing an object constructor instead will help with the process

function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokempnhealth");
	};
	this.faint = function(){
		console.lg(this.name + 'fainted')
	}
};

// Creates new instances of the new "Pokemon object each with their unique properties"
let pikachu = new Pokemon('Pikachu', 16);
let rattata = new Pokemon("ratata", 8);

// proving the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the 2 objects
pikachu.tackle(rattata);