// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all of the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
// Connecting to MongoDB atlas
mongoose.connect("mongodb+srv://antonio-255:admin123@zuitt-bootcamp.wce9aon.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	} 
);

app.use("/tasks", taskRoute);

// Server Listening
if(require.main === module){
	app.listen(port, () => console.log(`Server Running at ${port}`))
}

module.exports = app;