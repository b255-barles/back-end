const http = require('http');

// Creates a variable "port" to store the port number
const port = 4000;

// creates a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hello again')
	} else if (request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the home page')
	} else{
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not available')
	}

});
// Uses the "server" and "port" variables created above
server.listen(port);

console.log(`Server now accessbile at localhost: ${port}`);



