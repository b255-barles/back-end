// use the "require" directive to load node.js module
// A "module" is a software component or part of a progrm that contains one or more routines
//  the "HTTP module" lets node.js transfer data using the Hyper text transfer Protocol
//  HTTP is a protocol tht allows th fetching of resources such as HTML documents
// Clients(browser) and servers(nodejs/express js application) communicate by exchanging individual messages
// The messages sent by the client, usually a web browser, are called requests
//  The messages sent by the server as an answer are called responses

let http = require("http");

// Using this module's createServer() method, we can create an HTTP sever that listens to reqyuests on a specified port and gives responses
// the http module has a createServer() method that accepts a function as an argument and allows for a creating of a server
//  the arguments passed in the function are request and response objects(data type) that contains methods that allows us to reveice requests from the client and send responses back

http.createServer(function(request, response){

	// The HTTP method of the incomign request can be accessed via the "method" property of the "request" parameter
	//  The method "GET" means that we will be retrieveing or reading informationn
	if(request.url == "/items" && request.method == "GET"){
		// Requests the  "/items" path and "GETS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		// Ends the response process
		response.end('Data retrieved from the DataBase');
	}
	if(request.url == "/items" && request.method == "POST"){
		// The method "POST" means that we will be adding or creating information
		//  In this example, we will just be sending a text response for now
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the DataBase');
	}

}).listen(4000);

console.log('Server is running at localhost:4000')

