let http = require("http");

http.createServer(function(request, response){
	// localhost:4000/
	if(request.url == "/" && request.method == "GET"){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		
		response.end('Welcome to Booking system');
	}
	// localhost:4000/profile
	if(request.url == "/profile" && request.method == "GET"){
	
		response.writeHead(200, {'Content-Type': 'text/plain'});
		
		response.end('Welcome to your profile!');
	}
	// localhost:4000/courses
	if(request.url == "/courses" && request.method == "GET"){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		
		response.end("Here's our courses available");
	}
	// localhost:4000/addcourse
	if(request.url == "/addcourse" && request.method == "POST"){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		
		response.end('Add a course to our resources');
	}
	// localhost:4000/updatecourse
	if(request.url == "/updatecourse" && request.method == "PUT"){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		
		response.end('Updata a course to our resources');
	}
	// localhost:4000/archivecourses
	if(request.url == "/archivecourses" && request.method == "DELETE"){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		
		response.end('Archive courses to our resources');
	}
}).listen(4000);

console.log("server is running");