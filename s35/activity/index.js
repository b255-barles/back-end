const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 4000;

// [SECTION] MongoDB connection
mongoose.connect("mongodb+srv://antonio-255:admin123@zuitt-bootcamp.wce9aon.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);


let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

const userSchema = new mongoose.Schema({
	
	userName: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

const User = mongoose.model("User", userSchema);


app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
	User.findOne({userName : req.body.userName}).then((result, err) => {

		if(result != null && result.userName == req.body.userName){
			// Return a message to the client/Postman
			return res.send("Duplicate username found");

		// If no document was found
		}else {
			if(req.body.userName != null && req.body.password != null && req.body.userName != "" && req.body.password != ""){
			let newUser = new User({
				userName : req.body.userName,
				password : req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr);

				// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New user registered");

				}
			});
				}else{
					return res.send("BOTH username and password must be provided");
				}


			
			
		}
	})
})

app.get("/users", (req,res) => {
	User.find({}).then((result,err) => {
		if(err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;