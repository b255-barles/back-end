/*console.log("Hello World");*/
let firstName = 'John';
let lastName = 'Smith';
let age = 30;
let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
let workAddress= {
	houseNumber: '32',
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska',
}
/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:
/*console.log('First Name:' + firstName);
console.log('Last Name:' + lastName);
console.log('Age: ' + age);
console.log('Hobbies: ');
console.log(hobbies);
console.log('Work Address: ');
console.log(workAddress);*/

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	/*let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ['Tony','Bruce', 'Thor' ,'Natasha', 'Clint', 'Nick'];
	console.log("My Friends are: ");
	console.log(friends);
	

	let fullProfile = {
		userName: 'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,
	}
	console.log("My Full Profile:");
	console.log(fullProfile);
	

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	const lastLocation = "Arctic Ocean";
	// /*lastLocation = "Atlantic Ocean";*/
	// console.log("I was found frozen in: " + lastLocation);

const arr = [1,2,3,4,5];
const A = arr.filter((num) => num % 2 === 0).map((num) => num*2);
console.log(A)

	//Do not modify
		//For exporting to test.js
		try{
			module.exports = {
				firstName, lastName, age, hobbies, workAddress,
				fullName, currentAge, friends, profile, fullName2, lastLocation
			}
		} catch(err){
		}
