console.log('Hello World')

// Functions

	// Functions in JavaScript are lines/blocks f codes that tell our device/application to perform a certain task

	// Functions are mostly created to create complicated tasks to run several lines of code in succession

// Function Declaration

	function printName(){
		
		console.log("My name is John")

	}

// Function Invocation
	printName();

// Function Declaratins vs. expressions

	// Function declaration

	// A function can be created through function declaration by using the function keyword and adding a function name

	function declaredFunction(){
		console.log("Hello World from declaredFunction()")
	}

	declaredFunction();

	// Function Expression
	// A function can also be stored in a variable called Function expression
	// A fucntion expression is an anonymous function assigned to the variableFunction


	// Anonymous function - a function w/o a name

	let variableFunction = function(){
		console.log("Hello Again");
	}

	variableFunction();


	// We can also create a function expression of a name function
	// However to invoke the function exp. we invokeit by its var name bot by its func name


	let funcExpression = function funcName(){
		console.log("Hello")
	}

	funcExpression();

	// You can reassign declared functions and func expressions to new anonymous functions

	declaredFunction = function(){
		console.log("Updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function(){
		console.log ("Updated funcExpression")
	}

	funcExpression();

	// However, we cannot reaassign a func exp. initialized with const

	const constantFunc = function(){
		console.log("Initialized with const!")
	}

	constantFunc();

	/*constantFunc = function(){
		console.log("nani")
	}

	constantFunc();*/

// Function Scoping

/*
	scope is the accessibility of var within our prog

	JS variables has 3 types of scope
	1. Local/block scope
	2. global scope
	3. function scope
*/

	{
		let localVar = "Armando Perez"
	}
	let globalVar = "Mr. Worldwide";
	console.log(globalVar);

	// Function Scope

	function showNames(){

		var functionVar = "John"
		const functionConst = "Joe"
		let functionLet ="Jane"

		console.log(functionVar)
		console.log(functionConst)
		console.log(functionLet)

	}

	showNames();

	// Nested function

		// You can create another function inside a function. This is called a nested function.

		function myNewFunction(){
			let name = "Jane"

			function nestedFunction(){
				let nestedName = "John"
				console.log(name)
			} 

			nestedFunction();
		}

		myNewFunction();

	// Function and Gloal Scope Variables
		// Global Scoped Variables
		let globalName = "Alex";

		function myNewFunction2(){
			let nameInside = "Renz";
			console.log(globalName);
		};

		myNewFunction2();

	// Using alert()

		// alert() allows us to show a small window at the top of our browser page to show info to our users as opposed to console.log

		alert("Hello");

		function showSampleAlert(){
			alert("Hello, User")
		}

		showSampleAlert();


		console.log("I will only log in the console when the alert is dismissed");

	//  Using prompt() 
	//  prompt() allows us to show a small wndow at the browser to gather user inpur. It, much like alert() will have the page wait until dismissed

		let samplePrompt = prompt ("Enter your name: ");
		// alert("Hello" + samplePrompt);
		console.log ("Hello, " + samplePrompt);




		function printWelcomeMessage() {
			let firstName = prompt("Enter your First Name");
			let lastName = prompt("Enter your Last Name");
			
			console.log("Hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!")

		};

		printWelcomeMessage();

		// Function Naming Convention

		// Function names should be definitive of the task it will perform. it usually contains a verb.

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"]

			console.log
		};

		getCourses();

		// Avoid generic names to avoid confusion

		function get(){
			let name = "jamie";
			console.log(name);
		}

		get();

		// Avoid pointless and inappropriate function names

		function foo(){
			console.log(25%5)
		};

		foo();

		// Name your function in small caps. Follow camelCase when naming variables and functions

		function displayCarInfo(){
			console.log("Brand: Toyota")
			console.log("Type: Sedan")
			console.log("price: 1.5M")
		}

		displayCarInfo();



